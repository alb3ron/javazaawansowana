package com.sda.przyklad1;

@FunctionalInterface
public interface Example {

    int execute(int a, int b);

}
